package model;

public class Departamento {
	private String sigla;
	private String nome;
	private int numEmpregados;
	
	public Departamento() { }
	
	/**
	 * @param sigla
	 * @param nome
	 * @param numEmpregados
	 */
	public Departamento(String sigla, String nome, int numEmpregados) {
		super();
		this.sigla = sigla;
		this.nome = nome;
		this.numEmpregados = numEmpregados;
	}

	public String getSigla() {
		return sigla;
	}

	public void setSigla(String sigla) {
		this.sigla = sigla;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public int getNumEmpregados() {
		return numEmpregados;
	}

	public void setNumEmpregados(int numEmpregados) {
		this.numEmpregados = numEmpregados;
	}

	@Override
	public String toString() {
		return "Departamento [sigla=" + sigla + ", nome=" + nome + ", numEmpregados=" + numEmpregados + "]";
	}
	
	

}
