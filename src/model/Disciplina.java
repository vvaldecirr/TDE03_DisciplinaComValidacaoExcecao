package model;

/**
 * 
 * @author Valdecir
 *
 */
public class Disciplina {
	
	private String codigo;
	private String nome;
	private int numCreditos;

	/**
	 * Simple Constructor
	 */
	public Disciplina() {

	}

	/**
	 * Constructor
	 * 
	 * @param codigo
	 * @param nome
	 * @param numCreditos
	 * @throws DadosException 
	 */
	public Disciplina(String codigo, String nome, int numCreditos) throws DadosException {
		this.setCodigo(codigo);
		this.setNome(nome);
		this.setNumCreditos(numCreditos);
	}
	
	/**
	 * M�todo de valida��o onde o c�digo deve ser composto de 6 caracteres, sendo os tr�s primeiros letras
	 * mai�sculas e os tr�s �ltimos formados por d�gitos num�ricos (ex. EIN235)
	 * 
	 * @param codigo
	 * @throws DadosException
	 */
	public static void validarCodigo(String codigo) throws DadosException {
		if (codigo.length() < 6)
			throw new DadosException(new ErroDeDominio(1, Disciplina.class, "O campo c�digo precisa te no m�numo 6 caracteres"));
	}
	
	/**
	 * M�todo de valida��o onde o nome n�o pode ser nulo ou vazio
	 * 
	 * @param nome
	 * @throws DadosException
	 */
	public static void validarNome(String nome) throws DadosException {
		if (nome == "" || nome == null)
			throw new DadosException(new ErroDeDominio(1, Disciplina.class, "O campo nome n�o pode ficar em branco"));
	}

	/**
	 * M�todo de valida��o onde o numCreditos deve ser inteiro entre 2 e 6
	 * 
	 * @param nome
	 * @throws DadosException
	 */
	public static void validarNumCreditos(int numCreditos) throws DadosException {
		if ((numCreditos % 1) == 0 || numCreditos < 2 || numCreditos > 6)
			throw new DadosException(new ErroDeDominio(1, Disciplina.class, "O campo numCreditos deve ser inteiro entre 2 e 6"));
	}

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) throws DadosException {
		Disciplina.validarCodigo(codigo);
		this.codigo = codigo;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) throws DadosException {
		Disciplina.validarNome(nome);
		this.nome = nome;
	}

	public int getNumCreditos() {
		return numCreditos;
	}

	public void setNumCreditos(int numCreditos) throws DadosException {
		Disciplina.validarNumCreditos(numCreditos);
		this.numCreditos = numCreditos;
	}

}
